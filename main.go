package main

import (
	"time"

	"sync"

	"encoding/json"

	"bytes"
	"log"
	"net/http"

	"bitbucket.org/sergsoloviev/conf"
	"bitbucket.org/sergsoloviev/logger"
	"github.com/shirou/gopsutil/disk"
	"github.com/shirou/gopsutil/host"
	"github.com/shirou/gopsutil/mem"
)

type Stats struct {
	sync.Mutex
	Host    string  `json:"host"`
	Machine string  `json:"machine"`
	Mem     float64 `json:"mem"`
	Disk    float64 `json:"disk"`
}

func sendStats(js []byte, url string) {
	req, err := http.NewRequest("POST", url, bytes.NewBuffer(js))
	req.Header.Set("Content-Type", "application/json")
	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		log.Println(err, req)
		return
	}
	defer resp.Body.Close()
	if resp.StatusCode != 200 {
		log.Printf("Can't connect to %s, StatusCode: %d", url, resp.StatusCode)
	}
}

func (o *Stats) getDisk() {
	d, err := disk.Usage("/")
	if err == nil {
		o.Disk = d.UsedPercent
	}
}

func (o *Stats) getMem() {
	m, err := mem.VirtualMemory()
	if err == nil {
		o.Mem = m.UsedPercent
	}
}

func (o *Stats) getHost() {
	h, err := host.Info()
	if err == nil {
		o.Host = h.Hostname
	}
}

func (o *Stats) setMachine(name string) {
	o.Machine = name
}

func main() {
	Conf := conf.Read("/etc/go/agent/config.json")
	logger.SetLogs(Conf.Logs.Log, Conf.Logs.Output)

	s := Stats{}
	s.setMachine(Conf.Machine.Hostname)
	s.getHost()
	for {
		s.getMem()
		s.getDisk()
		jsBytes, _ := json.Marshal(s)
		go sendStats(jsBytes, Conf.Logs.Remote)
		time.Sleep(time.Second * time.Duration(Conf.Logs.Interval))
	}
}
