#!/usr/bin/env bash

NAME="agent"
VERSION="0.0.3"
COMPANY="atk24"
EMAIL="serg.soloviev@gmail.com"
DESCRIPTION="mem, disk usage monitoring"
APT_SERVER=""
SERVICE_URL="https://monitoring.example.com"
SERVICE_IP=""
SERVICE_PORT=""
SERVICE_USER="web"

#-------------------------------------------------------------------------------
cd ../
echo "building:"
go get ./...
GOOS=linux GOARCH=amd64 go build -v -o build/linux/$NAME main.go
# -ldflags="-s -w"
# upx --brute $NAME
#-------------------------------------------------------------------------------
echo "deb package dir..."
cd build/
rm $NAME*.deb
rm -rf debian
mkdir -p debian/DEBIAN
mkdir -p debian/var/log/$NAME
mkdir -p debian/etc/go/$NAME
#mkdir -p debian/etc/nginx/conf.d
mkdir -p debian/usr/local/$NAME/bin
#mkdir -p debian/usr/local/$NAME/static
#mkdir -p debian/usr/local/$NAME/templates
#mkdir -p debian/usr/local/$NAME/email
mkdir -p debian/lib/systemd/system
cp linux/$NAME debian/usr/local/$NAME/bin
touch debian/var/log/$NAME/$NAME.log
#cp -R ../static/* debian/usr/local/$NAME/static/
#cp -R ../templates/* debian/usr/local/$NAME/templates/
#cp -R ../email/* debian/usr/local/$NAME/email/
#-------------------------------------------------------------------------------
echo "service conf files:"
echo -e "\t/DEBIAN/control"
echo "Package: $NAME
Version: $VERSION
Maintainer: $COMPANY <$EMAIL>
Architecture: all
Description: $DESCRIPTION
" >> debian/DEBIAN/control

echo -e "\t/DEBIAN/postinst"
echo "#!/bin/sh

echo \"starting $NAME...\n\"
sudo service $NAME start
sudo systemctl enable $NAME.service
" >> debian/DEBIAN/postinst

echo -e "\t/DEBIAN/prerm"
echo "#!/bin/sh

echo \"stoping $NAME...\n\"
sudo service $NAME stop
" >> debian/DEBIAN/prerm

echo -e "\t/lib/systemd/system/$NAME.service"
echo "[Unit]
Description=$NAME service
After=network.target

[Service]
Type=simple
ExecStart=/usr/local/$NAME/bin/$NAME
KillMode=process
Restart=on-failure
RestartSec=10s
EnvironmentFile=/etc/go/$NAME/env

[Install]
WantedBy=multi-user.target
" >> debian/lib/systemd/system/$NAME.service

echo -e "\t/etc/go/$NAME/config.json"
echo '{
    "logs": {
        "log": "/var/log/agent.log",
        "output": true,
        "remote": "'$SERVICE_URL'",
        "interval": 300
    },
	"machine": {
		"hostname": "machine1"
	}
}
' >> debian/etc/go/$NAME/config.json

echo -e "\t/etc/go/$NAME/env"
echo 'GOPATH=/home/'$SERVICE_USER'/go
' >> debian/etc/go/$NAME/env

#-------------------------------------------------------------------------------
chmod 755 debian/DEBIAN/postinst
chmod 755 debian/DEBIAN/prerm
#-------------------------------------------------------------------------------

echo "building deb package"
fakeroot dpkg-deb --build debian .
#dpkg-deb --build debian .

echo "move deb package to apt repo..."
# scp $NAME*.deb $APT_SERVER:/var/www/debrepo/dist
cp $NAME*.deb /var/www/debrepo/dist
